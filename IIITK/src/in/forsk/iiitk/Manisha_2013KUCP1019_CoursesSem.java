//2013KUCP1019 MANISHA GHUGHARWAL
package in.forsk.iiitk;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
 

public class Manisha_2013KUCP1019_CoursesSem extends Activity {
 ListView lv;
    ArrayList<String>semlist=new ArrayList<String>();
    ArrayAdapter<String>ad;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.manisha_2013kucp1019_coursessem);
		lv = (ListView)findViewById(R.id.listViewSemester);
		semlist.add("Semester 1");
		semlist.add("Semester 2");
		semlist.add("Semester 3");
		semlist.add("Semester 4");
		semlist.add("Semester 5");
		semlist.add("Semester 6");
		semlist.add("Semester 7");
		semlist.add("Semester 8");
		ad = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,semlist); //?
		System.out.println(ad);
		System.out.println(lv);
		lv.setAdapter(ad);
		 lv.setOnItemClickListener(new OnItemClickListener() {
			 @Override
			 public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				Intent i=new Intent(Manisha_2013KUCP1019_CoursesSem.this,Manisha_2013KUCP1019_Courses.class);
				//i.putExtra("position",position);
				Manisha_2013KUCP1019_Courses.id=position;
				startActivity(i);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.manisha_2013_kucp1019__courses_sem,menu);
		return true;
	}

}
