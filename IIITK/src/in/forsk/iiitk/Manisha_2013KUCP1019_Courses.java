//2013KUCP1019 MANISHA GHUGHARWAL
package in.forsk.iiitk;

import java.io.ByteArrayOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;

import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;

public class Manisha_2013KUCP1019_Courses extends Activity {
	private final static String TAG = Manisha_2013KUCP1019_Courses.class
			.getSimpleName();
	Context context;
	int sem_position;
	TextView etUrl;
	static int id;
	String response = "";
	String url;
	Manisha_2013KUCP1019_CoursesAdapter mAdapter;
	// list view reference
	ListView lv;
	// arraylist holding objects of coursewrapper
	ArrayList<Manisha_2013KUCP1019_CoursesWrapper> mCourseDataList = new ArrayList<Manisha_2013KUCP1019_CoursesWrapper>();;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_manisha_2013_kucp1019__courses);
		sem_position = id;

		context = this;
		lv = (ListView) findViewById(R.id.listViewMain); // ?

		parseCourseList("http://online.mnit.ac.in/iiitk/assets/courses.json");
	}

	public void fun(View v) {
		finish();
	}

	private void parseCourseList(String url) {
		// TODO Auto-generated method stub
		this.url = url;
		new CustomAsyncTask().execute(url);
	}

	private String openHttpConnection(String urlStr) throws IOException {
		InputStream in = null;
		int resCode = -1;

		try {
			URL url = new URL(this.url);
			URLConnection urlConn = url.openConnection();

			if (!(urlConn instanceof HttpURLConnection)) {
				throw new IOException("URL is not an Http URL");
			}

			HttpURLConnection httpConn = (HttpURLConnection) urlConn;
			httpConn.setAllowUserInteraction(false);
			httpConn.setInstanceFollowRedirects(true);
			httpConn.setRequestMethod("GET");
			httpConn.connect();

			resCode = httpConn.getResponseCode();
			if (resCode == HttpURLConnection.HTTP_OK) {
				in = httpConn.getInputStream();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return convertStreamToString(in);
	}

	// Params, Progress, Result
	class CustomAsyncTask extends AsyncTask<String, Integer, String> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

		}

		@Override
		protected String doInBackground(String... params) {

			try {

				response = openHttpConnection(url);

				onProgressUpdate(0);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return response;

		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			mCourseDataList = parseLocalCourseFile(response);

			setCourseListAdapter(mCourseDataList);

		}

		public void setCourseListAdapter(
				ArrayList<Manisha_2013KUCP1019_CoursesWrapper> DataList) {
			mAdapter = new Manisha_2013KUCP1019_CoursesAdapter(context,
					R.layout.manisha_2013kucp1019_courseslistview, DataList);

			lv.setAdapter(mAdapter);
		}

	}

	private String convertStreamToString(InputStream is) throws IOException {
		// Converting input stream into string
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int i = is.read();
		while (i != -1) {
			baos.write(i);
			i = is.read();
		}
		return baos.toString();
	}

	public ArrayList<Manisha_2013KUCP1019_CoursesWrapper> parseLocalCourseFile(
			String json_string) {

		ArrayList<Manisha_2013KUCP1019_CoursesWrapper> mFacultyDataList = new ArrayList<Manisha_2013KUCP1019_CoursesWrapper>();
		try {
			// Converting multiple json data (String) into Json array
			JSONArray facultyArray = new JSONArray(json_string);
			Log.d(TAG, facultyArray.toString());
			// Iterating json array into json objects
			for (int i = 0; facultyArray.length() > i; i++) {

				// Extracting json object from particular index of array
				JSONObject facultyJsonObject = facultyArray.getJSONObject(i);

				// Design patterns
				Manisha_2013KUCP1019_CoursesWrapper facultyObject = new Manisha_2013KUCP1019_CoursesWrapper(
						facultyJsonObject);

				printObject(facultyObject);

				if (facultyObject.getsem() == sem_position + 1)
					mFacultyDataList.add(facultyObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mFacultyDataList;
	}

	private void printObject(Manisha_2013KUCP1019_CoursesWrapper obj) {
		// TODO Auto-generated method stub
		Log.d(TAG, "semester : " + obj.getsem());
		Log.d(TAG, "course_code : " + obj.getccode());
		Log.d(TAG, "course_name : " + obj.getcname());

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.manisha_2013_kucp1019__courses, menu);
		return true;
	}

}
