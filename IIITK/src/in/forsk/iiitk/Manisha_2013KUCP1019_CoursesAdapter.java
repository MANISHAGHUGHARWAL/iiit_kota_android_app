//2013KUCP1019 MANISHA GHUGHARWAL
package in.forsk.iiitk;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.androidquery.AQuery;

public class Manisha_2013KUCP1019_CoursesAdapter extends BaseAdapter {
private final static String TAG = Manisha_2013KUCP1019_CoursesAdapter.class.getSimpleName();
	// context to init the layout inflater service , which inflate the custom
	// Views(Resource layout)
	Context context;
	// layout resource id for the row layout(single row view)
	int resource;
	// to hold the data model (Instance variable )
	// as best practice ,data which is coming from an outside scope by
	// method/constructors
	// need to be hold for the lifetime of the class object
	// Similar we have created variable for holding context and resource id
	ArrayList<Manisha_2013KUCP1019_CoursesWrapper> mCourseDataList;
	// Instantiates a layout XML file into its corresponding View objects.
	// In other words, it takes as input an XML file and builds the View objects
	// from it.
	LayoutInflater inflater;
	// 3rd party library to do our dirty work(background threading)
	AQuery aq;
	public Manisha_2013KUCP1019_CoursesAdapter(Context context, int resource,
			ArrayList<Manisha_2013KUCP1019_CoursesWrapper> objects) {
		this.context = context;
		this.resource = resource;
		this.mCourseDataList = (ArrayList<Manisha_2013KUCP1019_CoursesWrapper>) objects;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		aq = new AQuery(context);

	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHoder holder;
		if (convertView == null) {
			convertView = inflater.inflate(resource, null);
			Log.d(TAG, "New View init - " + position);
			holder = new ViewHoder(convertView);
			convertView.setTag(holder);

		} else {
			Log.d(TAG, "Recycling old Views - " + position);

			// we've just avoided calling findViewById() on resource everytime
			// just use the viewHolder
			holder = (ViewHoder) convertView.getTag();
		}
		// Retrieve the data at particular index(position)
		// so we can bind the correct data
		Manisha_2013KUCP1019_CoursesWrapper obj = (Manisha_2013KUCP1019_CoursesWrapper) getItem(position);

		holder.courseCode.setText(obj.getccode());
		holder.courseName.setText(obj.getcname());

	 Log.d(TAG, "get View - " + position);

		return convertView;
	}

	// The ViewHolder design pattern enables you to access each list item view
	// without the need for the look up,
	// saving valuable processor cycles. Specifically, it avoids frequent call
	// of findViewById() during ListView
	// scrolling, and that will make it smooth.

	public static class ViewHoder {

		TextView courseCode, courseName;

		public ViewHoder(View view) {

			courseCode = (TextView) view.findViewById(R.id.courseCode);
			courseName = (TextView) view.findViewById(R.id.courseName);

		}
		}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		// This is called internally by the get view method
		// this is the max number on rows adapter created
		return mCourseDataList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		// this return object at particular position
		// we can use this method to apply custom functionality or filtration
		return mCourseDataList.get(position);

	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		// this method is use to maintain uniqueness of each row
		return position;
	}


}
